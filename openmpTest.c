#include <stdio.h>
#include <mpi.h>
#include <math.h>
#include <stdlib.h>
#include <omp.h>

#define RESULT 1
#define FINISH 2
#define SERIAL MPI_THREAD_SERIALIZED
#define MULTI MPI_THREAD_MULTIPLE

void master(int proccount) {
	printf("\nMaster\n");

	MPI_Status status;
	int i, result;

	for (i = 0; i < 4 * (proccount-1); i++) {
		MPI_Recv(&result, 1, MPI_INT, MPI_ANY_SOURCE, RESULT, MPI_COMM_WORLD, &status);
		printf("\nMessage from %d\n", result);
	}
}

void slave(int myrank) {
	MPI_Status status;
	int np, iam, sendBuf = 0;

#pragma omp parallel shared(myrank) private(iam, np) firstprivate(sendBuf)
	{
		np = omp_get_num_threads();
		iam = omp_get_thread_num();
		printf("Hello from thread %d out of %d from process %d\n", iam, np, myrank);
		sendBuf += myrank * 10 + iam;
#pragma omp critical
		{
			MPI_Send(&sendBuf, 1, MPI_INT, 0, RESULT, MPI_COMM_WORLD);
		}
	}

}

int main(int argc, char **argv) {

	int myrank, proccount, providedSupportLevel;

	MPI_Init_thread(&argc, &argv, SERIAL, &providedSupportLevel);
	if (providedSupportLevel != SERIAL) {
		printf("Not supported");
	}
	MPI_Comm_rank(MPI_COMM_WORLD, &myrank);
	MPI_Comm_size(MPI_COMM_WORLD, &proccount);
	
	if (myrank == 0) {
		master(proccount);
	}
	else {
		slave(myrank);
	}

	MPI_Finalize();
	return 0;
}