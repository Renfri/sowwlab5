#include <stdio.h>
#include <mpi.h>
#include <math.h>
#include <stdlib.h>

#define PRECISION 0.000001
#define RANGESIZE 1
#define DATA 0
#define RESULT 1
#define FINISH 2
#define DATA_PER_NODE 8
#define PI_MULTIPLIER 8

struct queue
{
    double info[2];
    struct queue *link;
};
struct queue *start=NULL;
int queueCounter=0;

void enqueue(double data[])
{
    struct queue *new;
    new=(struct queue *)malloc(sizeof(struct queue));
    new->info[0] = data[0];
    new->info[1] = data[1];
    new->link=start;
    start=new;
    queueCounter++;
}

void dequeue(double* data){
    if(queueCounter==0){
        data[0] = 0;
        data[1] = 0;
        return;
    }
    data[0] = start->info[0];
    data[1] = start->info[1];
    struct queue *temp;
    temp = start;
    start = start->link;
    queueCounter--;
    free(temp);
}

double f(double x) {
    return sin(x);
}

double SimpleIntegration(double a,double b) {
    double i;
    double sum=0;
    for (i = a; i < b; i += PRECISION)
        sum += f(i)*PRECISION;
    return sum;
}

void master(int proccount, float a, float b) {

	MPI_Request request, sendRequest;
	MPI_Status status;

	double sendBuffs[proccount][2], range[2], initialData[DATA_PER_NODE][2], resulttemp, result;
	int balance = 0, i = 0, j = 0;
	range[0] = a;
	range[1] = range[0];

	// first distribute some ranges to all slaves
	for (i = 1; i < proccount; i++) {
		for (j = 0; j < DATA_PER_NODE; j++) {
			range[1] = range[0] + RANGESIZE;
			initialData[j][0] = range[0];
			initialData[j][1] = range[1];
			range[0] = range[1];
			balance++;
		}
		MPI_Send(initialData, DATA_PER_NODE * 2, MPI_DOUBLE, i, DATA, MPI_COMM_WORLD);

#ifdef DEBUG
		fflush(stdout);
		printf("\nSend ranges from %f to %f to process %d",initialData[0][0], initialData[DATA_PER_NODE - 1][1], i);
#endif
	}

	if (range[1] < b) {
		do {
			MPI_Irecv(&resulttemp, 1, MPI_DOUBLE, MPI_ANY_SOURCE, RESULT, MPI_COMM_WORLD, &request);
			MPI_Wait(&request, &status);

#ifdef DEBUG
			fflush(stdout);
			printf("\nRecv result %f from process %d",resulttemp, status.MPI_SOURCE);
#endif

			result += resulttemp;
			range[1] = range[0] + RANGESIZE;
			if (range[1] > b) {
				range[1] = b;
			}
			sendBuffs[status.MPI_SOURCE][0] = range[0];
			sendBuffs[status.MPI_SOURCE][1] = range[1];
			MPI_Isend(sendBuffs[status.MPI_SOURCE], 2, MPI_DOUBLE, status.MPI_SOURCE, DATA, MPI_COMM_WORLD, &request);
			MPI_Request_free(&request);

#ifdef DEBUG
			fflush(stdout);
			printf("\nSend range %f,%f to process %d",sendBuffs[status.MPI_SOURCE][0],sendBuffs[status.MPI_SOURCE][1], status.MPI_SOURCE);
#endif

			range[0] = range[1];
		} while (range[1] < b);
	}

	while (balance > 0) {
		MPI_Recv(&resulttemp, 1, MPI_DOUBLE, MPI_ANY_SOURCE, RESULT, MPI_COMM_WORLD, &status);
		balance--;

#ifdef DEBUG
		fflush(stdout);
		printf("\nRecv result %f from process %d",resulttemp, status.MPI_SOURCE);
#endif

		result += resulttemp;
	}

	// shut down the slaves
	for (i = 1; i < proccount; i++) {
		MPI_Send(NULL, 0, MPI_DOUBLE, i, FINISH, MPI_COMM_WORLD);
	}

	fflush(stdout);
	printf("\nHi, I am process 0, the result is %.12f\n", result);
}


void slave(int myrank) {

	MPI_Status status;
	MPI_Request sendRequest, recvRequest;

	double range[2], initialData[DATA_PER_NODE][2], recvBuf[2], resulttemp;
	int i = 0, recvCompleted = 0, sendCompleted = 1;

#ifdef DEBUG
	fflush(stdout);
	printf("\nSlave %d started", myrank);
#endif

	MPI_Recv(initialData, DATA_PER_NODE * 2, MPI_DOUBLE, 0, DATA, MPI_COMM_WORLD, &status);
	for (i = 0; i < DATA_PER_NODE; i++) {
#ifdef DEBUG
		fflush(stdout);
		printf("\nRecv ranges %f, %f, in process %d\n", initialData[i][0], initialData[i][1], myrank);
#endif
		enqueue(initialData[i]);
	}
	int iam = 0, np = 1;

	do {
		if (queueCounter > 0) {
			if (!sendCompleted) {
				MPI_Wait(&sendRequest, MPI_STATUS_IGNORE);
				sendCompleted = 1;
			}
			dequeue(range);

			
			resulttemp = SimpleIntegration(range[0], range[1]);


			MPI_Isend(&resulttemp, 1, MPI_DOUBLE, 0, RESULT, MPI_COMM_WORLD, &sendRequest);
#ifdef DEBUG
			fflush(stdout);
			printf("\nSending result %f from ranges %f, %f, from process %d\n", resulttemp, range[0], range[1], myrank);
#endif
			sendCompleted = 0;
		}

			MPI_Iprobe(0, MPI_ANY_TAG, MPI_COMM_WORLD, &recvCompleted, &status);
			if (recvCompleted && status.MPI_TAG != FINISH) {
				MPI_Recv(recvBuf, 2, MPI_DOUBLE, 0, DATA, MPI_COMM_WORLD, &status);
				enqueue(recvBuf);
			}
	} while (status.MPI_TAG != FINISH);

	while (queueCounter > 0) {
		dequeue(range);
		resulttemp = SimpleIntegration(range[0], range[1]);
		MPI_Send(&resulttemp, 1, MPI_DOUBLE, 0, RESULT, MPI_COMM_WORLD);
	}

#ifdef DEBUG
	fflush(stdout);
	printf("Process %d finished", myrank);
#endif

}

int main(int argc, char **argv) {

	double a = 0, b = PI_MULTIPLIER*M_PI;
	int myrank, proccount, providedSupportLevel;

	MPI_Init(&argc, &argv);
    MPI_Comm_rank(MPI_COMM_WORLD, &myrank);
    MPI_Comm_size(MPI_COMM_WORLD, &proccount);

    if (proccount<2) {
        printf("Run with at least 2 processes");
        MPI_Finalize();
        return -1;
    }
    if (((b-a)/RANGESIZE)<2*(proccount-1)) {
        printf("More subranges needed");
        MPI_Finalize();
        return -1;
    }
    if (((b-a)/RANGESIZE) < DATA_PER_NODE*(proccount-1)) {
        printf("Not enough ranges to give every process %i ranges.", DATA_PER_NODE);
        MPI_Finalize();
        return -1;
    }

    if (myrank==0) {
		master(proccount, a, b);
    }     
    else {
		slave(myrank);

    }

    MPI_Finalize();
    return 0;
}